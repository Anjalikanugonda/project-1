package com.page.locators;

import org.openqa.selenium.By;

public interface amazonhomelocators {

	By SELEC =By.id("searchDropdownBox");
	By SEARC= By.className("nav-input");
	By MEN= By.xpath(".//*[@id='nav-subnav']/a[3]/span[1]");
	By SHIRTS= By.xpath("(//a[.='Shirts'])[1]");
	By dropdown1=By.xpath("//span[contains(.,'GHPC')]");
	By dropdown2= By.xpath("//label/span/span[.='GHPC']");
	By titles=By.xpath("//div/a/h2");
}

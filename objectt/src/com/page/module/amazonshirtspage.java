package com.page.module;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.page.locators.amazonshirtslocators;
import com.page.module.amazonhomepage;
public class amazonshirtspage implements amazonshirtslocators{
	private WebDriver driver;
	public amazonshirtspage(WebDriver driv){
		driver=driv;
	}
	public void clickShirts(){
		Actions examp=new Actions(driver);
		examp.moveToElement(driver.findElement(MEN)).build().perform();
		WebDriverWait wait=new WebDriverWait(driver,5);
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(SHIRTS))).click();
}
}
